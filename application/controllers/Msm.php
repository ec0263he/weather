<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Msm
 * @property Msm_model $msm_model
 * @property CI_Input $input
 * @property CI_Output $output
 */
class Msm extends CI_Controller
{

    /** @var float $lat */
    private $lat;
    /** @var float $lng */
    private $lng;
    /** @var float $lat_end */
    private $lat_end;
    /** @var float $lat_start */
    private $lat_start;
    /** @var float $lat_step */
    private $lat_step;
    /** @var float $lng_start */
    private $lng_start;
    /** @var float $lng_end */
    private $lng_end;
    /** @var float $lng_step */
    private $lng_step;
    /** @var float $lat_num */
    private $lat_num;
    /** @var float $lng_num */
    private $lng_num;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('msm_model');
        $this->lat = (float)$this->input->get('lat');
        $this->lng = (float)$this->input->get('lng');

        if (substr(__FUNCTION__, 0, 3) === 'msm') {
            $this->lat_end = 47.6;
            $this->lat_start = 22.4;
            $this->lat_step = 0.05;
            $this->lng_start = 120.0;
            $this->lng_end = 150.0;
            $this->lng_step = 0.0625;
            $this->lat_num = 505;
            $this->lng_num = 481;
        } else if (__FUNCTION__ === 'nowc') {
            $this->lat_end = 47.995833;
            $this->lat_start = 20.004167;
            $this->lat_step = 0.008333;
            $this->lng_start = 118.006250;
            $this->lng_end = 149.993750;
            $this->lng_step = 0.012500;
            $this->lat_num = 3360;
            $this->lng_num = 2560;
        }
    }

    /**
     * MSM 1時間降水量を出力
     */
    public function msm_apcp()
    {
        $bin_path = '/home/ubuntu/bindata/msm_apcp.bin';
        $hour_offset = $this->lat_num * $this->lng_num * 4;

        $lat_cnt = (int)(($this->lat - $this->lat_start) / $this->lat_step);
        $lng_cnt = (int)(($this->lng - $this->lng_start) / $this->lng_step);

        $filesize = filesize($bin_path);
        $hour_num = $filesize / $hour_offset;
        $fp = fopen($bin_path, 'rb');
        $offset = 4 * ($this->lng_num * $lat_cnt + $lng_cnt);
        fseek($fp, $offset);
        $dataArr = [];
        for ($i=0; $i<$hour_num; $i++) {
            if($i>0){
                fseek($fp, $hour_offset-4, SEEK_CUR);
            }
            $data = fread($fp, 4);
            $dataArr[] = unpack('ffloat', $data)['float'];
        }
        rewind($fp);
        fclose($fp);

        if ($this->input->get('format') && $this->input->get('format') === 'json') {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['data' => $dataArr]));
        } else {
            $this->load->view('weather_table', ['msm_apcp' => $dataArr]);
        }
    }

    /**
     * 5分間降水ナウキャスト
     * 5分ごとの降水強度を出力
     */
    public function nowc()
    {
        $bin_path = '/home/ubuntu/bindata/nowc.bin';
        $hour_offset = $this->lat_num * $this->lng_num * 4;

        $lat_cnt = (int)(($this->lat - $this->lat_start) / $this->lat_step);
        $lng_cnt = (int)(($this->lng - $this->lng_start) / $this->lng_step);

        $filesize = filesize($bin_path);
        $hour_num = $filesize / $hour_offset;
        $fp = fopen($bin_path, 'rb');
        $offset = 4 * ($this->lng_num * $lat_cnt + $lng_cnt);
        fseek($fp, $offset);
        $dataArr = [];
        for ($i=0; $i<$hour_num; $i++) {
            if($i>0){
                fseek($fp, $hour_offset-4, SEEK_CUR);
            }
            $data = fread($fp, 4);
            $temp_data = unpack('ffloat', $data)['float'];
            $dataArr[] = $temp_data <=1000 ? $temp_data : 0.0;
        }
        rewind($fp);
        fclose($fp);

        if ($this->input->get('format') && $this->input->get('format') === 'json') {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['data' => $dataArr]));
        } else {
            $this->load->view('nowc_table', ['nowc' => $dataArr]);
        }
    }

    /**
     * MSM 雲量出力
     */
    public function msm_tcdc()
    {
        $bin_path = '/home/ubuntu/bindata/msm_tcdc.bin';
        $hour_offset = $this->lat_num * $this->lng_num * 4;

        $lat_cnt = (int)(($this->lat - $this->lat_start) / $this->lat_step);
        $lng_cnt = (int)(($this->-lng - $this->lng_start) / $this->lng_step);

        $filesize = filesize($bin_path);
        $hour_num = $filesize / $hour_offset;
        $fp = fopen($bin_path, 'rb');
        $offset = 4 * ($this->lng_num * $lat_cnt + $lng_cnt);
        fseek($fp, $offset);
        $dataArr = [];
        for ($i=0; $i<$hour_num; $i++) {
            if($i>0){
                fseek($fp, $hour_offset-4, SEEK_CUR);
            }
            $data = fread($fp, 4);
            $tcdc_value = unpack('ffloat', $data)['float'];
            if ($tcdc_value < 20) {
                $tcdc_str = '快晴';
            } else if ($tcdc_value < 80) {
                $tcdc_str = '晴';
            } else {
                $tcdc_str = '曇';
            }
            $dataArr[] = [
                'value' => $tcdc_value,
                'string' => $tcdc_str
            ];
        }
        rewind($fp);
        fclose($fp);

        if ($this->input->get('format') && $this->input->get('format') === 'json') {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['data' => $dataArr]));
        } else {
            $this->load->view('weather_table', ['msm_tcdc' => $dataArr]);
        }
    }

    /**
     * MSM 風速を出力
     * U成分（西→東の風）、V成分（南→北の風）、方角、風速
     */
    public function msm_wind()
    {
        $ugrd_bin_path = '/home/ubuntu/bindata/msm_ugrd.bin';
        $vgrd_bin_path = '/home/ubuntu/bindata/msm_vgrd.bin';
        $hour_offset = $this->lat_num * $this->lng_num * 4;

        $lat_cnt = (int)(($this->lat - $this->lat_start) / $this->lat_step);
        $lng_cnt = (int)(($this->lng - $this->lng_start) / $this->lng_step);

        $filesize = filesize($ugrd_bin_path);
        $hour_num = $filesize / $hour_offset;
        $ugrd_fp = fopen($ugrd_bin_path, 'rb');
        $vgrd_fp = fopen($vgrd_bin_path, 'rb');
        $offset = 4 * ($this->lng_num * $lat_cnt + $lng_cnt);
        fseek($ugrd_fp, $offset);
        fseek($vgrd_fp, $offset);
        $dataArr = [];
        for ($i=0; $i<$hour_num; $i++) {
            if($i>0){
                fseek($ugrd_fp, $hour_offset-4, SEEK_CUR);
                fseek($vgrd_fp, $hour_offset-4, SEEK_CUR);
            }
            $udata = fread($ugrd_fp, 4);
            $vdata = fread($vgrd_fp, 4);

            $degree = $this->get_wind_degree(unpack('ffloat', $udata)['float'],unpack('ffloat', $vdata)['float']);
            $velocity = $this->get_wind_velocity(unpack('ffloat', $udata)['float'],unpack('ffloat', $vdata)['float']);



            $dataArr[] = [
                'UGRD' => unpack('ffloat', $udata)['float'],
                'VGRD' => unpack('ffloat', $vdata)['float'],
                'DEG' => $degree,
                'VELOCITY'   => $velocity,
                'DIRECTION'  => $this->deg2dir($degree)
            ];
        }
        rewind($ugrd_fp);
        rewind($vgrd_fp);
        fclose($ugrd_fp);
        fclose($vgrd_fp);

        if ($this->input->get('format') && $this->input->get('format') === 'json') {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(['data' => $dataArr]));
        } else {
            $this->load->view('weather_table', ['msm_wind' => $dataArr]);
        }
    }

    /**
     * 北から時計周りに計った角度から方角を計算
     * @param float $degree
     * @return string
     */
    private function deg2dir(float $degree)
    {
        if ($degree < 11.25 && $degree >= 348.75) {
            return '北'; // 0
        } else if ($degree < 33.75) {
            return '北北東'; // 22.5
        } else if ($degree < 56.25) {
            return '北東';  // 45
        } else if ($degree < 78.75) {
            return '東北東';
        } else if ($degree < 101.25) {
            return '東';
        } else if ($degree < 123.75) {
            return '東南東';
        } else if ($degree < 146.25) {
            return '南東';
        } else if ($degree < 168.75) {
            return '南南東';
        } else if ($degree < 191.25) {
            return '南';
        } else if ($degree < 213.75) {
            return '南南西';
        } else if ($degree < 236.25) {
            return '南西';
        } else if ($degree < 258.75) {
            return '西南西';
        } else if ($degree < 281.25) {
            return '西';
        } else if ($degree < 303.75) {
            return '西北西';
        } else if ($degree < 326.25) {
            return '北西';
        } else if ($degree < 348.75) {
            return '北北西';
        }


    }

    /**
     * U成分、V成分の風速から合計された風速を計算
     * @param float $ugrd
     * @param float $vgrd
     * @return float
     */
    private function get_wind_velocity(float $ugrd, float $vgrd)
    {
        return sqrt(pow($ugrd, 2) + pow($vgrd, 2));
    }

    /**
     * U成分、V成分の風速から方角（北を0として時計回りを計算
     * @param float $ugrd
     * @param float $vgrd
     * @return float|int
     */
    private function get_wind_degree(float $ugrd, float $vgrd)
    {
        if ($vgrd == 0) {
            if ($ugrd > 0) {
                return 270;
            } else {
                return 90;
            }
        } else if ($ugrd == 0) {
            if ($vgrd > 0) {
                return 180;
            } else {
                return 0;
            }
        }
        else if ($ugrd >= 0 && $vgrd >= 0) {
            return rad2deg(atan($ugrd / $vgrd) + pi());
        } else if ($ugrd <= 0 && $vgrd >= 0) {
            return rad2deg(atan($ugrd / $vgrd) + pi());
        } else if ($ugrd <= 0 && $vgrd <= 0) {
            return rad2deg(atan($ugrd / $vgrd));
        } else if ($ugrd >= 0 && $vgrd <= 0) {
            return rad2deg(atan($ugrd / $vgrd) + 2 + pi());
        }
    }

    public function read_bin()
    {
        if(!$this->input->is_cli_request())exit();
        $lat_end = 47.6;
        $lat_start = 22.4;
        $lat_step = 0.05;
        $lng_start = 120.0;
        $lng_end = 150.0;
        $lng_step = 0.0625;
        $lat_num = 505;
        $lng_num = 481;

        $argv = $_SERVER['argv'];
        $bin_path = $argv[3];
        $lat = $argv[4];
        $lng = $argv[5];
        $hour_offset = $lat_num * $lng_num * 4;

        $lat_cnt = (int)(($lat - $lat_start) / $lat_step);
        $lng_cnt = (int)(($lng - $lng_start) / $lng_step);

        $filesize = filesize($bin_path);
        $hour_num = $filesize / $hour_offset;
        $fp = fopen($bin_path, 'rb');
        $offset = 4 * ($lng_num * $lat_cnt + $lng_cnt);
        echo 'initial_offset: ' . $offset;

        $dataArr = [];
        for ($i=0; $i<$hour_num; $i++) {
            if($i>0){
                fseek($fp, $hour_offset-4, SEEK_CUR);
            }
            $data = fread($fp, 4);
            $dataArr[] = unpack('f', $data);
        }
        rewind($fp);
        fclose($fp);
        var_dump($dataArr);
    }


    public function store($v)
    {
        $argv = $_SERVER['argv'];
        $full_path = $argv[3];
        $file_path = pathinfo($full_path);
        $csv_full_path = $file_path['dirname'] . '/' . $file_path['filename'] . '.csv';

        $lng_start = 120.0;
        $lng_end = 150.0;
        $lng_step = 0.0625;
        $lat_start = 22.4;
        $lat_end = 47.6;
        $lat_step = 0.05;
        $hour_end = 24;



        if ($fp = fopen($full_path, 'r')) {
            $fp = fopen($csv_full_path, 'r');
            $lat = $lat_start;
            $lng = $lng_start;
            $hour = 0;
            while (($row = fgetcsv($fp)) !== FALSE) {
                $fcst_hour = 9+$hour !== 24 ? sprintf("%02d", 9 + $hour) : '00';
                $data = [
                    'lat' => $lat,
                    'lng' => $lng,
                    'apcp_' . $fcst_hour => (float)$row[6]
                ];
                $this->msm_model->store($data);
                if ($lng < $lng_end) {
                    $lng += $lng_step;
                } else if ($lat < $lat_end) {
                    $lat += $lat_step;
                    $lng = $lng_start;
                } else if ($hour < $hour_end){
                    $hour++;
                    $lng = $lng_start;
                    $lat = $lat_start;
                } else {
                    break;
                }
            }

        } else {
            echo 'file open error';
        }
    }


}
