<?php
/**
 * Class Msm_model
 * @property CI_DB_mysqli_driver $db
 */
class Msm_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store($data)
    {
        if($this->db->get_where('msm_00z', ['lat' => $data['lat'], 'lng' => $data['lng']])->result()) {
            $this->db->update('msm_00z' , $data, ['lat' => $data['lat'], 'lng' => $data['lng']]);
        } else {
            $this->db->insert('msm_00z', $data);
        }
    }

}
